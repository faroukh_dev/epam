import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-parent',
    templateUrl: './parent.component.html',
    styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
    }

    showChild(id) {
        this.router.navigate([`./child${id}`], {
            queryParams: {
                name: 'Scot'
            },
            relativeTo: this.activatedRoute
        });
    }

}
