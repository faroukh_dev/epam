import {Injectable} from '@angular/core';
import {
    ActivatedRouteSnapshot, CanActivate, CanActivateChild, CanDeactivate, Route, Router,
    RouterStateSnapshot
} from '@angular/router';
import {ParentComponent} from '../../components/parent/parent.component';

@Injectable()
export class ParentGuard implements CanActivate, CanDeactivate<ParentComponent>, CanActivateChild {

    private secret: string = 'secret';

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot,
                state: RouterStateSnapshot): boolean {

        console.log('canActivate route: ', route);
        console.log('canActivate state: ', state);

        return true;
    }

    canDeactivate(component: ParentComponent,
                  currentRoute: ActivatedRouteSnapshot,
                  currentState: RouterStateSnapshot,
                  nextState: RouterStateSnapshot) {

        console.log('canDeactivate component: ', component);
        console.log('canDeactivate currentRoute: ', currentRoute);
        console.log('canDeactivate currentState: ', currentState);
        console.log('canDeactivate nextState: ', nextState);

        return true;

    }

    canActivateChild(route: ActivatedRouteSnapshot,
                     state: RouterStateSnapshot): boolean {

        console.log('canActivateChild route: ', route);
        console.log('canActivateChild state: ', state);
        return true;
    }
}
