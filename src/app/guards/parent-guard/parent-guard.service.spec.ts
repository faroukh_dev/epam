import { TestBed, inject } from '@angular/core/testing';

import { ParentGuard } from './parent-guard.service';

describe('ParentGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ParentGuard]
    });
  });

  it('should be created', inject([ParentGuard], (service: ParentGuard) => {
    expect(service).toBeTruthy();
  }));
});
