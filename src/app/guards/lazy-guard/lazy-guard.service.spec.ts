import { TestBed, inject } from '@angular/core/testing';

import { LazyGuard } from './lazy-guard.service';

describe('LazyGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LazyGuard]
    });
  });

  it('should be created', inject([LazyGuard], (service: LazyGuard) => {
    expect(service).toBeTruthy();
  }));
});
