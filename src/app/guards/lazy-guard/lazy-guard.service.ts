import {Injectable} from '@angular/core';
import {CanLoad} from '@angular/router';

@Injectable()
export class LazyGuard implements CanLoad {

    constructor() {
    }

    canLoad(): boolean {
        return true;
    }

}
