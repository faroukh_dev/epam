import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {Page1Component} from './components/page1/page1.component';
import {Page2Component} from './components/page2/page2.component';
import {Page3Component} from './components/page3/page3.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {Child1Component} from './components/child1/child1.component';
import {Child2Component} from './components/child2/child2.component';
import {Child3Component} from './components/child3/child3.component';
import {ParentComponent} from './components/parent/parent.component';
import {ParentGuard} from './guards/parent-guard/parent-guard.service';
import {LazyComponent} from './components/lazy/lazy.component';
import {LazyGuard} from './guards/lazy-guard/lazy-guard.service';

const appRoutes: Routes = [
    {
        path: 'lazy',
        loadChildren: './lazy/lazy.module#LazyModule',
        canLoad: [LazyGuard]
    }, {
        path: 'page1',
        component: Page1Component
    }, {
        path: 'page2',
        component: Page2Component,
        data: {
            name: 'John'
        }
    }, {
        path: 'page3/:param',
        component: Page3Component
    }, {
        path: '',
        redirectTo: '/page2',
        pathMatch: 'full'
    }, {
        path: 'parent',
        component: ParentComponent,
        canActivate: [ParentGuard],
        canDeactivate: [ParentGuard],
        canActivateChild: [ParentGuard],
        children: [{
            path: 'child1',
            component: Child1Component
        }, {
            path: 'child2',
            component: Child2Component
        }, {
            path: 'child3',
            component: Child3Component
        }
        ]
    }, {
        path: '**',
        component: PageNotFoundComponent
    }
];

@NgModule({
    declarations: [
        AppComponent,
        Page1Component,
        Page2Component,
        Page3Component,
        PageNotFoundComponent,
        Child1Component,
        Child2Component,
        Child3Component,
        ParentComponent
    ],
    imports: [
        RouterModule.forRoot(appRoutes),
        BrowserModule
    ],
    providers: [ParentGuard, LazyGuard],
    bootstrap: [AppComponent]
})
export class AppModule {
}
